package ru.natera.graph;

import ru.natera.graph.impl.DfsPathFinder;

import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public class GraphImpl<V> implements Graph<V> {
    private final GraphStorage<V> graphStorage;
    private final BiFunction<V, V, Edge<V>> edgeCreator;
    private final DfsPathFinder<V> pathFinder;

    public GraphImpl(GraphStorage<V> graphStorage, BiFunction<V, V, Edge<V>> edgeCreator, DfsPathFinder<V> pathFinder) {
        this.graphStorage = graphStorage;
        this.edgeCreator = edgeCreator;
        this.pathFinder = pathFinder;
    }

    @Override
    public V addVertex(V value) {
        graphStorage.addVertex(value);
        return value;
    }

    @Override
    public Edge<V> addEdge(V fromVertex, V toVertex) {
        Edge<V> edge = edgeCreator.apply(fromVertex, toVertex);
        edge.getConnectedVertexStream()
                .forEach(pair -> graphStorage.addEdge(pair.getFromVertex(), pair.getToVertex()));
        return edge;
    }

    @Override
    public Stream<Edge<V>> getPath(V fromVertex, V toVertex) {
        return pathFinder.getPath(graphStorage, edgeCreator, fromVertex, toVertex);
    }
}
