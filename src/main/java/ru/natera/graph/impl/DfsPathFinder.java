package ru.natera.graph.impl;

import ru.natera.graph.Edge;
import ru.natera.graph.GraphStorage;
import ru.natera.graph.PathFinderAlgo;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public class DfsPathFinder<V> implements PathFinderAlgo<V> {
    @Override
    public Stream<Edge<V>> getPath(
            GraphStorage<V> graphStorage,
            BiFunction<V, V, Edge<V>> edgeCreator,
            V from,
            V to) {

        Set<V> visitedVertexes = new HashSet<>();
        Stack<V> vertexesInPath = new Stack<>();
        vertexesInPath.add(from);
        visitedVertexes.add(from);

        while (visitedVertexes.size() < graphStorage.vertexesCount() && !vertexesInPath.empty()) {
            V currentVertex = vertexesInPath.peek();
            if (currentVertex.equals(to)) { //if we already find the path, we should return it
                return createPath(vertexesInPath, edgeCreator);
            }
            List<V> linkedVertices = graphStorage.getLinkedVertexes(currentVertex);
            if (linkedVertices.isEmpty()) {//if this vertex has no linked vertexes, we remove this vertex from the path
                vertexesInPath.pop();
                visitedVertexes.add(currentVertex);
            }
            else {
                boolean shouldPopAtTheEnd = true;
                for (V linkedVertex : linkedVertices) {
                    if (!visitedVertexes.contains(linkedVertex)) {
                        shouldPopAtTheEnd = false;
                        visitedVertexes.add(linkedVertex);
                        vertexesInPath.add(linkedVertex);
                        break;
                    }
                }
                if (shouldPopAtTheEnd) {//if all linked vertexes are visited, we remove this vertex from the path
                    vertexesInPath.pop();
                }
            }
        }
        if (!vertexesInPath.empty() && vertexesInPath.peek().equals(to)) {
            return createPath(vertexesInPath, edgeCreator);
        }

        return Stream.empty();
    }

    private Stream<Edge<V>> createPath(Stack<V> vertexesInPath, BiFunction<V, V, Edge<V>> edgeCreator) {
        if (vertexesInPath.size() == 1) {
            return Stream.of(edgeCreator.apply(vertexesInPath.peek(), vertexesInPath.peek()));
        }
        List<Edge<V>> edges = new ArrayList<>();
        Collections.reverse(vertexesInPath);
        while (!vertexesInPath.empty()) {
            V from = vertexesInPath.pop();
            if (!vertexesInPath.empty()) {
                edges.add(edgeCreator.apply(from, vertexesInPath.peek()));
            }
        }

        return edges.stream();
    }
}
