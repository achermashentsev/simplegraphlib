package ru.natera.graph;

import org.junit.Test;
import ru.natera.graph.impl.BiDirectionalEdge;
import ru.natera.graph.impl.DfsPathFinder;
import ru.natera.graph.impl.GraphStorageImpl;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author a.chermashentsev
 * Date: 05.07.2020
 **/
public class UndirectedGraphTest {

    @Test
    public void find_path_in_two_vertex_graph() {
        GraphImpl<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);

        graph.addEdge(1,2);

        List<Edge<Integer>> path1 = graph.getPath(vertex1, vertex2).collect(Collectors.toList());
        assertEquals(1, path1.size());
        assertEquals(Utils.createEdgeList(BiDirectionalEdge::new, 1,2), path1);

        List<Edge<Integer>> path2 = graph.getPath(vertex2, vertex1).collect(Collectors.toList());
        assertEquals(1, path2.size());
        assertEquals(Utils.createEdgeList(BiDirectionalEdge::new, 2,1), path2);
    }

    @Test
    public void find_path_in_branched_graph() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);

        //1 -> 2
        //1 -> 3 -> 4 -> 5
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);

        List<Edge<Integer>> path = graph.getPath(2, 5).collect(Collectors.toList());
        assertEquals(4, path.size());
        assertEquals(Utils.createEdgeList(BiDirectionalEdge::new, 2,1,3,4,5), path);
    }

    @Test
    public void find_path_in_cycled_graph() {

        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);

        //1 -> 2
        //1 -> 3 -> 4 -> 5 -> 2
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);
        graph.addEdge(vertex5, vertex2);

        List<Edge<Integer>> path = graph.getPath(vertex2, vertex5).collect(Collectors.toList());
        //Path doesn't have to be optimal, so there are two possible paths
        assertTrue(
                String.format("Incorrect path between vertexes 2 and 5: %s", path),
                Utils.createEdgeList(BiDirectionalEdge::new, 2,1,3,4,5).equals(path) ||
                        Utils.createEdgeList(BiDirectionalEdge::new, 2,5).equals(path)
        );
    }

    @Test
    public void should_not_find_path_for_unreachable_vertex() {

        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);
        Integer vertex6 = graph.addVertex(6);
        Integer vertex7 = graph.addVertex(7);

        //1 -> 2
        //1 -> 3 -> 4 -> 5
        //6 -> 7
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);
        graph.addEdge(vertex5, vertex2);
        graph.addEdge(vertex6, vertex7);

        List<Edge<Integer>> path = graph.getPath(vertex1, vertex7).collect(Collectors.toList());
        //Path doesn't have to be optimal, so there are two possible paths
        assertEquals(0, path.size());
    }

    private GraphImpl<Integer> createGraph() {
        return new GraphImpl<>(new GraphStorageImpl<>(), BiDirectionalEdge::new, new DfsPathFinder<>());
    }
}
