package ru.natera.graph.impl;

import ru.natera.graph.GraphStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public class GraphStorageImpl<V> implements GraphStorage<V> {
    private final Map<V, List<V>> vertexesLinks = new HashMap<>();

    @Override
    public void addVertex(V vertex) {
        vertexesLinks.put(vertex, new ArrayList<>());
    }

    @Override
    public void addEdge(V fromVertex, V toVertex) {
        List<V> list = vertexesLinks.computeIfAbsent(fromVertex, ignore -> new ArrayList<>());
        list.add(toVertex);
    }

    @Override
    public List<V> getLinkedVertexes(V vertex) {
        return vertexesLinks.get(vertex);
    }

    @Override
    public int vertexesCount() {
        return vertexesLinks.size();
    }
}
