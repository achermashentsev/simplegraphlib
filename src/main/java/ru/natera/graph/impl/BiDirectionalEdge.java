package ru.natera.graph.impl;

import ru.natera.graph.ConnectedVertexPair;
import ru.natera.graph.Edge;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public class BiDirectionalEdge<V> implements Edge<V> {
    private final V from;
    private final V to;

    public BiDirectionalEdge(V from, V to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public Stream<ConnectedVertexPair<V>> getConnectedVertexStream() {
        return Stream.of(
                new ConnectedVertexPairImpl<>(from, to),
                new ConnectedVertexPairImpl<>(to, from)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BiDirectionalEdge<?> that = (BiDirectionalEdge<?>) o;
        return (Objects.equals(from, that.from) &&
                Objects.equals(to, that.to)) ||
                (Objects.equals(from, that.to) && Objects.equals(to, that.from));
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    @Override
    public String toString() {
        return "BiDirectionalEdge{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
