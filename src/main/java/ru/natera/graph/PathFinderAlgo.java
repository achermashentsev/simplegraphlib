package ru.natera.graph;

import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * Interface for an algorithm for finding path in the graph
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public interface PathFinderAlgo<V> {

    /**
     * Should find path between two vertexes in graph.
     * @param graphStorage storage for graph
     * @param edgeCreator function for create edge between two vertexes
     * @param from from vertex
     * @param to to vertex
     * @return path between from and to vertexes
     */
    Stream<Edge<V>> getPath(
            GraphStorage<V> graphStorage,
            BiFunction<V, V, Edge<V>> edgeCreator,
            V from,
            V to
    );
}
