package ru.natera.graph;

import org.junit.Test;
import ru.natera.graph.impl.DfsPathFinder;
import ru.natera.graph.impl.GraphStorageImpl;
import ru.natera.graph.impl.OneDirectionalEdge;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static ru.natera.graph.Utils.createEdgeList;

/**
 * @author a.chermashentsev
 * Date: 01.07.2020
 **/
public class DirectedGraphTest {

    @Test
    public void find_path_in_two_vertex_graph() {
        Graph<String> graph = new GraphImpl<>(new GraphStorageImpl<>(), OneDirectionalEdge::new, new DfsPathFinder<>());
        String helloVertex = graph.addVertex("hello");
        String worldVertex = graph.addVertex("world");

        //hello -> world
        Edge<String> stringEdge = graph.addEdge(helloVertex, worldVertex);

        List<Edge<String>> edges = graph.getPath(helloVertex, worldVertex)
                .collect(Collectors.toList());
        assertEquals( 1, edges.size());
        assertEquals(stringEdge, edges.get(0));

    }

    @Test
    public void find_path_in_branched_graph() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);
        Integer vertex6 = graph.addVertex(6);
        Integer vertex7 = graph.addVertex(7);

        //1 -> 2 -> 4
        //     2 -> 7
        //1 -> 3 -> 5 -> 6
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex2, vertex4);
        graph.addEdge(vertex2, vertex7);
        graph.addEdge(vertex3, vertex5);
        graph.addEdge(vertex5, vertex6);

        List<Edge<Integer>> path = graph.getPath(vertex1, vertex6).collect(Collectors.toList());
        assertEquals(3, path.size());
        assertEquals(createEdgeList(OneDirectionalEdge::new, 1, 3, 5, 6), path);

    }

    @Test
    public void find_path_in_cycled_graph() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);

        //1 -> 2
        //1 -> 3 -> 4 -> 5 -> 1
        //
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);
        graph.addEdge(vertex5, vertex1);

        List<Edge<Integer>> path = graph.getPath(vertex1, vertex5).collect(Collectors.toList());
        assertEquals(3, path.size());
        assertEquals(createEdgeList(OneDirectionalEdge::new, 1,3,4,5), path);
    }

    @Test
    public void find_path_in_graph_with_unreachable_vertex() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);
        Integer vertex6 = graph.addVertex(6);

        //1 -> 2
        //1 -> 3 -> 4 -> 5
        //6(not connected)
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);

        List<Edge<Integer>> path = graph.getPath(vertex3, vertex5).collect(Collectors.toList());

        assertEquals(2, path.size());
        assertEquals(createEdgeList(OneDirectionalEdge::new, 3,4,5), path);
    }

    @Test
    public void should_not_find_path_for_not_connected_vertexes() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);

        //1 -> 2
        //1 -> 3 -> 4 -> 5
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);

        List<Edge<Integer>> path = graph.getPath(vertex2, vertex5).collect(Collectors.toList());

        assertEquals(0, path.size());
    }

    @Test
    public void should_not_find_path_for_unreachable_vertex() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);
        Integer vertex3 = graph.addVertex(3);
        Integer vertex4 = graph.addVertex(4);
        Integer vertex5 = graph.addVertex(5);
        Integer vertex6 = graph.addVertex(6);

        //1 -> 2
        //1 -> 3 -> 4 -> 5
        //6(not connected)
        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex1, vertex3);
        graph.addEdge(vertex3, vertex4);
        graph.addEdge(vertex4, vertex5);

        List<Edge<Integer>> path = graph.getPath(vertex6, vertex1).collect(Collectors.toList());
        assertEquals(0, path.size());
    }

    @Test
    public void should_find_path_for_not_connected_vertexes1() {
        Graph<Integer> graph = createGraph();
        Integer vertex1 = graph.addVertex(1);
        Integer vertex2 = graph.addVertex(2);

        //1 -> 2
        graph.addEdge(vertex1, vertex2);

        List<Edge<Integer>> path = graph.getPath(vertex2, vertex1).collect(Collectors.toList());
        assertEquals(0, path.size());
    }

    private GraphImpl<Integer> createGraph() {
        return new GraphImpl<>(new GraphStorageImpl<>(), OneDirectionalEdge::new, new DfsPathFinder<>());
    }
}
