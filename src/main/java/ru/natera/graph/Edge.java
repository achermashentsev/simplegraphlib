package ru.natera.graph;

import java.util.stream.Stream;

/**
 * Edge between two vertexes
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public interface Edge<V> {
    /**
     * @return stream of connected vertexes
     */
    Stream<ConnectedVertexPair<V>> getConnectedVertexStream();
}
