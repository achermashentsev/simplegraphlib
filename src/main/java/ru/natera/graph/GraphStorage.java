package ru.natera.graph;

import java.util.List;

/**
 * Storage for graph
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public interface GraphStorage<V> {

    /**
     * Adds edge to storage
     */
    void addEdge(V fromVertex, V toVertex);

    /**
     * @param vertex vertex, for which you want to find linked vertexes
     * @return linked vertexes for passed vertex
     */
    List<V> getLinkedVertexes(V vertex);

    /**
     * Adds vertex to graph
     * @param vertex vertex for add
     */
    void addVertex(V vertex);

    /**
     * @return count of vertexes in graph
     */
    int vertexesCount();
}
