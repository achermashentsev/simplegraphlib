package ru.natera.graph;

/**
 * Interface for two connected vertexes
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public interface ConnectedVertexPair<V> {

    V getFromVertex();

    V getToVertex();
}
