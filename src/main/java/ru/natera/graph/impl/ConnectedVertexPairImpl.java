package ru.natera.graph.impl;

import ru.natera.graph.ConnectedVertexPair;

/**
 * @author a.chermashentsev
 * Date: 03.07.2020
 **/
public class ConnectedVertexPairImpl<V> implements ConnectedVertexPair<V> {
    private final V from;
    private final V to;

    public ConnectedVertexPairImpl(V from, V to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public V getFromVertex() {
        return from;
    }

    @Override
    public V getToVertex() {
        return to;
    }
}
