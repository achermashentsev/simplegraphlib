package ru.natera.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * @author a.chermashentsev
 * Date: 05.07.2020
 **/
public class Utils {

    @SafeVarargs
    public static  <T> List<Edge<T>> createEdgeList(BiFunction<T,T, Edge<T>> edgeCreator, T... vertexes) {
        if (vertexes.length < 2) {
            throw new IllegalArgumentException("Vertexes length must be more than 1");
        }
        ArrayList<Edge<T>> res = new ArrayList<>();
        for (int i = 0; i < vertexes.length-1; i++) {
            res.add(edgeCreator.apply(vertexes[i], vertexes[i + 1]));
        }
        return res;
    }
}
