package ru.natera.graph;


import java.util.stream.Stream;

/**
 * The main interface for graph.
 * @author a.chermashentsev
 * Date: 01.07.2020
 **/
public interface Graph<V> {

    /**
     * Adds vertex to graph.
     * Behavior for the case when vertex is already added, depends on concrete implementation
     * @param value vertex value
     * @return added vertex value
     */
    V addVertex(V value);

    /**
     * Adds edge between two vertexes.
     * Behavior for the case when edge is already created and for the case when @param{fromVertex} equals to @param{toVertex}, depends on concrete implementation
     * @return created edge
     */
    Edge<V> addEdge(V fromVertex, V toVertex);

    /**
     * Returns path between two vertexes.
     * If there is no path, returns empty stream.
     * @return path between two vertexes.
     */
    Stream<Edge<V>> getPath(V fromVertex, V toVertex);
}
